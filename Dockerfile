FROM php:7.4-apache

# Instalação das extensoes
RUN docker-php-ext-install mysqli

EXPOSE 80
EXPOSE 443

WORKDIR /var/www/html
